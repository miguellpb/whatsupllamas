#!/usr/bin/env python3
# Copyright (C) 2024 Daniel Steinhauer <d.steinhauer@mailbox.org>

from os import getenv
from functools import reduce
from sys import exit
import logging
import requests

from dotenv import load_dotenv
from atlassian import Confluence
from bs4 import BeautifulSoup

SPACES_LIMIT = 500
PAGES_LIMIT = 500

def fetch_space_keys(confl: Confluence, start: int = 0):
    """
    Fetch a list of unique identifier keys for every Confluence space.
    If not all spaces can be retrieved in one call, fetch the remaining ones recursively.

    :param confl: Confluence: The Confluence instance.
    :param start: int: OPTIONAL: The starting number for the spaces (only important for recursion).
    :return: A list of space keys as strings.
    """
    spaces = confl.get_all_spaces(start, SPACES_LIMIT)["results"]
    if len(spaces) == 0:
        return []
    else:
        return [sp["key"] for sp in spaces] + fetch_space_keys(confl, start + len(spaces))

def fetch_pages_from_space(confl: Confluence, space_key: str, start: int = 0):
    """
    Fetch all pages from a given space.
    Recursively fetch more if one request is not sufficient.

    :param confl: Confluence: The Confluence instance.
    :param space_key: str: The unique identifier key of the space.
    :param start: int: OPTIONAL: The starting number (only important for recursion).
    :return: A list of dicts containing the page link and its text.
    """
    pages = confl.get_space_content(space_key, start=start, limit=PAGES_LIMIT)["page"]["results"]
    if len(pages) == 0:
        return []
    else:
        return [
            {
                "link": confl.url + pg["_links"]["webui"],
                "text": BeautifulSoup(pg["body"]["storage"]["value"], "lxml").text
            }
            for pg in pages
        ] + fetch_pages_from_space(confl, space_key, start + len(pages))

class PrivateGPTConnection:
    def __init__(self, url: str):
        self.url = url
        self.ingest_list = []
        self.unique_files = {}

    def check_health(self):
        """
        Check the health of the PrivateGPT instance.

        :return: True if it is healthy, False otherwise.
        """
        r = requests.get(self.url + '/health')
        return r.json()["status"] == "ok"

    def fetch_ingested_list(self):
        """
        Fetch a list of ingested documents and save it internally.
        Also create a set of filenames.
        """
        r = requests.get(self.url + "/v1/ingest/list")
        self.ingest_list = r.json()["data"]
        self.unique_files = set([fl["doc_metadata"]["file_name"] for fl in self.ingest_list])

    def get_doc_ids_of_file(self, filename: str):
        """
        Get the *doc_id*s of a given filename.

        :param filename: str: The file name.
        :return: A list of doc_ids as strings.
        """
        return [fl["doc_id"] for fl in self.ingest_list if fl["doc_metadata"]["file_name"] == filename]

    def delete_file(self, filename: str):
        """
        Send DELETE requests for every doc_id associated with the given filename.

        :param filename: string: The file name to delete.
        """
        for doc_id in self.get_doc_ids_of_file(filename):
            r = requests.delete(self.url + '/v1/ingest/' + doc_id)
            if not r.ok:
                raise Exception(f"Error: Could not delete chunk {doc_id} of file {filename}.")

    def ingest_text(self, title: str, text: str):
        """
        Send a POST request to ingest the given text.

        :param title: str: The title a.k.a the file name of the given text blob.
        :param text: str: The text to ingest.
        """
        data = {
            "file_name": title,
            "text": text
        }
        r = requests.post(url=self.url + '/v1/ingest/text', json=data)
        if not r.ok:
            raise Exception(f"Error: Could not ingest text \"{title}\".")

    def update_text(self, title: str, text: str):
        """
        First check whether an ingested file with the given title already exists.
        If so, delete it first, then ingest the text (again).

        :param title: str: The title a.k.a. the file name.
        :param text: str: The text to ingest.
        """
        self.fetch_ingested_list()
        if title in self.unique_files:
            self.delete_file(title)
        self.ingest_text(title, text)

if __name__ == "__main__":
    if getenv("log") != None:
        logging.basicConfig(level=getenv("log"))

    # Load credentials from .env file
    # It needs the variables INSTANCE, TOKEN and PGPT_INST
    loaded = load_dotenv()
    if not loaded:
        logging.error("No .env file found or empty.")
        exit(1)

    if getenv("INSTANCE") == None:
        logging.error("You need to add \"INSTANCE\" to your _credentials.env_ or add as environment variable.")
        exit(1)

    if getenv("TOKEN") == None:
        logging.error("You need to add \"TOKEN\" to your _credentials.env_ or add as environment variable.")
        exit(1)

    if getenv("PGPT_INST") == None:
        logging.error("You need to add \"PGPT_INST\" to your _credentials.env_ or add as environment variable.")
        exit(1)

    # Establish a connection to Confluence instance
    confluence = Confluence(
        url=getenv("INSTANCE"),
        token=getenv("TOKEN")
    )

    gpt = PrivateGPTConnection(getenv("PGPT_INST"))

    if not gpt.check_health():
        logging.error("PrivateGPT instance is not healthy.")
        exit(1)

    try:
        spaces = fetch_space_keys(confluence)
        logging.info(f"Fetched {len(spaces)} spaces from Confluence.")
    except Exception as exc:
        logging.error("Failed to fetch spaces: %s\n", str(exc))
        exit(1)

    try:
        # Get a flat list of all pages this user has access to.
        all_pages = reduce(lambda first, sec: first + fetch_pages_from_space(confluence, sec), spaces, [])
        logging.info(f"Fetched {len(all_pages)} pages from Concluence.")
    except Exception as exc:
        logging.error("Failed to fetch pages: %s\n", str(exc))
        exit(1)

    for pg in all_pages:
        link, text = pg.values()
        logging.info(f"Ingesting {link}")
        gpt.update_text(link, text)

    logging.info("All pages successfully ingested. :-)")
