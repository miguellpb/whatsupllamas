# Whatsupllamas
This project aims to read pages from a _Confluence_ documentation and ingest it into [PrivateGPT](https://privategpt.dev).

## Preparation
### Python
First you need to have a recent version of python installed.
Then create a virtual environment for the script:
```sh
python3 -m venv ./.venv
```

Change into the new environment:
```sh
source ./.venv/bin/activate
```

Install dependencies:
```sh
pip3 install -r requirements.txt
```

### PrivateGPT
You need a working instance of [PrivateGPT](https://docs.privategpt.dev/overview/welcome/introduction).
Follow the [provided instructions](https://docs.privategpt.dev/installation/getting-started/installation)
to install if you haven't done so already.

### OPTIONAL: Setup a local Confluence instance
You can set up a local instance of _Confluence_ using docker-compose and this docker compose
file shamelessly taken from [here](https://community.atlassian.com/t5/Confluence-articles/Confluence-Deployment-using-Docker-Compose/ba-p/1898938):

```sh
docker-compose up
```

You still need to enter a license code you can obtain for free for 90 days.

### Credentials
You need to provide the script with some data about your configurations.
Namely you need the path to the _PrivateGPT_ instance, the path to the _Confluence_ instance
and an _Confluence_ access [token](https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html).

Create a file called **.env** and fill in the data:
```
INSTANCE=<URL of confluence>
TOKEN=<Your PAT>
PGPT_INST=<URL of PrivateGPT>
```

## Running
You can simply run the scraping script:
```sh
python3 scrape.py
```

If you need additional information:
```sh
log=INFO python3 scrape.py
```
